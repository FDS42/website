FROM golang:1.21-alpine3.18 as build

# Install Hugo. Note: this installs the extended version, which is required for the Doks starter theme
# Install Git, which is required for Hugo modules
# Install NPM, which is required for the Doks theme
RUN apk add --update hugo git npm

# Copy the repo content to /site
WORKDIR /site

COPY package.json package-lock.json ./

# Run npm install to build the dependencies
RUN npm install

COPY . .

# Run Hugo to build the static files
RUN hugo --gc

# Serve the files using nginx (preferred over serving them with the Hugo server)
FROM nginx:1.25-alpine3.17

COPY --from=build /site/public /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
