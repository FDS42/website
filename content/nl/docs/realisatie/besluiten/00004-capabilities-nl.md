---
title: 00004 Capabilities NL
date: 2023-11-07
author: Peter Bergman, Mike Dell, Marc van Andel, Gert-Jan Aaftink
categories: [DR]
tags: 
status: proposed
description: >
   We gebruiken de OpenDEI building blocks als capabilities,
   maar hoe vertalen we deze naar het Nederlands?
---

## Context en probleemstelling

Gegeven de context van de DR [00001 Basisstructuur](./00001-basisstructuur/) waarin we bepalen dat we
met capabilities gaan werken volgens de building blocks van OpenDEI, hoe noemen we de capabilities
dan in het Nederlands? Nemen we de OpenDEI building blocks letterlijk over? Geven we nog een andere
context aan onze capabilities? (Waarmee ook de vertaling naar het Engels anders wordt?)

## Beslissingsfactoren <!-- optional -->

- Context:
  - Doelstellingen [Realisatie Interbestuurlijke Datastrategie
    (IBDS)](https://realisatieibds.pleio.nl/)
  - Doelstellingen [Federatief Datastelsel
    (FDS)](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
- Decision Records:
  - DR [00001 Basisstructuur](../00001-basisstructuur/)
- OpenDEI:

## Overwogen opties

- [OpenDEI oorspronkelijk](#opendei-oorspronkelijk)
- [OpenDEI letterlijke vertaling](#opendei-letterlijke-vertaling)
- [Mike's vertaling](#mikes-vertaling)
- [Peter's vertaling](#peters-vertaling)
- [Capabilities sessies](#capabilities-sessies)

## Besluit

We kiezen voor een zo kort en eenvoudig mogelijke duiding en daarom nemen we voornamelijk [Mike's
vertaling](#mikes-vertaling) over. Daarbij voegen we wel een zin per capability / woord toe om te
zorgen voor een juiste en gedeelde 'lading' van elke capability.

- **Governance**
  - **Bestuurlijk**: Bestuurlijke overeenkomsten en juridische constructen
  - **Operationeel**: Operationele overeenkomsten en afspraken
  - **Beheer**: Continuïteit en doorontwikkeling van het stelsel
- **Interoperabiliteit**
  - **Modellen**: Data- en informatiemodellen en -formaten
  - **Dataservices**: API's voor data uitwisseling
  - **Traceerbaarheid**: Traceerbaarheid en herleidbaarheid
- **Datawaarde**
  - **Metadata**: Metadata in brede zin met beschrijvingen, verwijzingen en meer
  - **Verantwoording**: Verantwoording van datagebruik
  - **Publicatie**: Publicatie- en marktplaatsdiensten
- **Vertrouwen**
  - **Identiteit**: Identiteitsbeheer
  - **Toegang**: Toegangscontrole en gebruikscontrole
  - **Veiligheid**: Vertrouwde uitwisseling

### Positieve gevolgen <!-- optional -->

- … <!-- numbers of options can vary -->

### Negatieve Consequences <!-- optional -->

- … <!-- numbers of options can vary -->

## Pros en Cons van de oplossingen <!-- optional -->

### OpenDEI oorspronkelijk

- Governance
  - Overarching cooperation agreement
  - Operational (e.g. SLA)
  - Continuity model
- Interoperability
  - Data models and formats
  - Data exchange APIs
  - Provenance and traceability
- Data value
  - Metadata and Discovery Protocol
  - Data usage accounting
  - Publication and marketplace services
- Trust
  - Identity management
  - Access and usage control / policies
  - Trusted exchange

### OpenDEI letterlijke vertaling

- Bestuur
   - Overkoepelend samenwerkingsakkoord
   - Operationeel (bijv. SLA)
   - Continuïteitsmodel
- Interoperabiliteit
   - Datamodellen en formaten
   - API's voor gegevensuitwisseling
   - Herkomst en traceerbaarheid
- Gegevenswaarde
   - Metagegevens en ontdekkingsprotocol
   - Accounting van gegevensgebruik
   - Publicatie- en marktplaatsdiensten
- Vertrouwen
   - Identiteitsbeheer
   - Toegangs- en gebruikscontrole / beleid
   - Vertrouwde uitwisseling

### Mike's vertaling

- Governance
  - Bestuurlijk
  - Operationeel
  - Organisatie
- Interoperabiliteit
  - Modellen
  - Interoperabiliteit
  - Traceerbaarheid
- Data met waarde
  - Beschrijving
  - Verantwoording
  - Diensten
- Vertrouwen
  - Identiteit
  - Toegang
  - Veiligheid

FDS overwegingen:

- Enkel één woord is wel erg prettig!
- Enkel één woord geeft weinig 'lading' en verdient wel duidelijke beschrijvingen van wat er dan wel
  en niet onder valt. Hier ligt het gevaar dat 'men' alleen de woorden als uitgangspunt neemt en
  verschillende lading daar aan geeft. Hoewel een enkel woord eenvoudig lijkt en duidelijkheid lijkt
  te geven, zou het ook juist tot verwarring kunnen leiden. Dat is slechts een risico zo lang de
  woorden nog niet goed geladen zijn. Zodra dat wel het geval is, draagt het zeker bij aan een
  eenvoudig en duidelijk model!
- Interoperabiliteit > Interoperabiliteit ... is niet heel duidelijk. Zou de capability niet 'iets
  met API's' moeten bevatten ... alléén 'API's'?
- 'Data met waarde' is anders dan 'Data value'. Dat beschrijft meer de waarde _van_ data. De
  capabilities verwijzen meer naar de bekwaamheden die de data waarde gaan geven en/of duiden.
  Waarom niet directer vertalen met 'Data waarde'?
- Data ~~met~~ waarde > Beschrijving verwijst vooral naar de metadata. Is 'Metadata' beter dan
  'Beschrijving'?
- Data ~~met~~ waarde > Diensten gaat vooral over de publicatie en het gebruik van die publicatie in
  een marktplaats, terwijl diensten zelf ook kan duiden op de API's. Is 'Publicatie' beter?

### Peter's vertaling

- Governance
  - Standaard generieke samenwerkovereenkomst
  - Standaard operationele overeenkomst (SLA)
  - Regie op en continuïteit van het stelsel
- Interoperabiliteit
  - Datamodellen en -formaten
  - Datadeel services (API's)
  - Herkomst en traceerbaarheid
- Waarde van data
  - Protocol voor metadata en zoeken
  - Verantwoording data gebruik
  - Services voor datapublicatie en datamarktplaats
- Vertrouwen
  - Identiteit management
  - Beleid toegang en gebruik
  - Vertrouwd data delen

### Capabilities sessies

- Governance
  - [nog consolideren]
- Interoperabiliteit
  - Datamodellen en formaten
  - Data uitwisseling en APIs
  - Herleidbaarheid en traceerbaarheid
  - [extra] Ontwikkelen en beheren meta-data-standaarden
- Data waarde
  - Metadata en discovery
  - Verantwoording datagebruik
  - Publicatie en marktplaats-services
- Vertrouwen
  - Identiteitsbeheer
  - Toegangs- en gebruikerscontrole/beleid
  - Vertrouwd uitwisselen

FDS overwegingen:

Tijdens de capabilities sessies zijn we steeds afgeweken van de OpenDEI building blocks. Dat heeft
geleid tot goede discussies en uitwerking van diverse (bijv.) rollen van het federatief datastelsel.
Echter ... wijkt dat dusdanig af dat het niet meer herkenbaar is naar het oorspronkelijke model van
OpenDEI. Aangezien we in DR [00001 Basisstructuur](./00001-basisstructuur/) hebben besloten om juist
wél dicht bij het OpenDEI model te blijven, moeten we herzien wat we tijdens de sessies bedacht
hebben.

### Vergelijkingstabel

| OpenDEI oorspronkelijk               | OpenDEI letterlijke vertaling          | Mike's vertaling   | Peter's vertaling                               | Capabilities sessies                                 |
| ------------------------------------ | -------------------------------------- | ------------------ | ----------------------------------------------- | ---------------------------------------------------- |
| Governance                           | Bestuur                                | Governance         | Governance                                      | Governance                                           |
| Overarching cooperation agreement    | Overkoepelend samenwerkingsakkoord     | Bestuurlijk        | Standaard generieke samenwerkovereenkomst       | [nog consolideren]                                   |
| Operational (e.g. SLA)               | Operationeel (bijv. SLA)               | Operationeel       | Standaard operationele overeenkomst (SLA)       |
| Continuity model                     | Continuïteitsmodel                     | Organisatie        | Regie op en continuïteit van het stelsel        |
| Interoperability                     | Interoperabiliteit                     | Interoperabiliteit | Interoperabiliteit                              | Interoperabiliteit                                   |
| Data models and formats              | Datamodellen en formaten               | Modellen           | Datamodellen en -formaten                       | Datamodellen en formaten                             |
| Data exchange APIs                   | API's voor gegevensuitwisseling        | Interoperabiliteit | Datadeel services (API's)                       | Data uitwisseling en APIs                            |
| Provenance and traceability          | Herkomst en traceerbaarheid            | Traceerbaarheid    | Herkomst en traceerbaarheid                     | Herleidbaarheid en traceerbaarheid                   |
|                                      |                                        |                    |                                                 | [extra] Ontwikkelen en beheren meta-data-standaarden |
| Data value                           | Gegevenswaarde                         | Data met waarde    | Waarde van data                                 | Data waarde                                          |
| Metadata and Discovery Protocol      | Metagegevens en ontdekkingsprotocol    | Beschrijving       | Protocol voor metadata en zoeken                | Metadata en discovery                                |
| Data usage accounting                | Accounting van gegevensgebruik         | Verantwoording     | Verantwoording data gebruik                     | Verantwoording datagebruik                           |
| Publication and marketplace services | Publicatie- en marktplaatsdiensten     | Diensten           | Services voor datapublicatie en datamarktplaats | Publicatie en marktplaats-services                   |
| Trust                                | Vertrouwen                             | Vertrouwen         | Vertrouwen                                      | Vertrouwen                                           |
| Identity management                  | Identiteitsbeheer                      | Identiteit         | Identiteit management                           | Identiteitsbeheer                                    |
| Access and usage control / policies  | Toegangs- en gebruikscontrole / beleid | Toegang            | Beleid toegang en gebruik                       | Toegangs- en gebruikerscontrole/beleid               |
| Trusted exchange                     | Vertrouwde uitwisseling                | Veiligheid         | Vertrouwd data delen                            | Vertrouwd uitwisselen                                |

## Links <!-- optional -->

- DR [00001 Basisstructuur](./00001-basisstructuur/)
- doc [Raamwerk voor ontwikkeling in samenwerking](/docs/realisatie/raamwerk/)
