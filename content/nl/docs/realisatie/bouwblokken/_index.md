---
title: Bouwblokken
weight: 40
description: >
  De bouwblokken die de realisatie van de [capabilities](/docs/realisatie/capabilities/) vormen.
---
