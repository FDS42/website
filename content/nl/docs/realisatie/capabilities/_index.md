---
title: Capabilities
# menu: {main: {weight: 20}}
weight: 20
description: >
   De capabilities (bekwaamheden) van het federatief datastelsel met relaties (verwijzingen)
   naar de [bouwblokken](/docs/realisatie/bouwblokken/) die voor de realisatie nodig zijn
---

Het federatief datastelsel gebruikt een [Raamwerk voor ontwikkeling in
samenwerking](/docs/realisatie/raamwerk/). Hieronder is de uitwerking van de capabilities die daar
benoemd worden.

Klik op de betreffende capability om te lezen waar deze over gaat, welke componenten,
afhankelijkheden en relaties deze heeft.

{{< capabilities-diagram >}}

&nbsp;  

Voor meer informatie over
[Capabilities](/docs/realisatie/besluiten/00001-basisstructuur/#capabilities) en de context van het
federatief datastelsel, lees onze [Strategie van
samenwerken](/docs/welkom/strategie-van-samenwerken/) en het [Raamwerk voor ontwikkeling in
samenwerking](/docs/realisatie/raamwerk/). In de [Werkomgeving](/docs/welkom/werkomgeving/) kun je
lezen hoe je kunt bijdragen en hoe de samenwerkomgeving is opgezet. Wil je meer lezen over
achterliggende overwegingen, lees onze DR [00001
Basisstructuur](/docs/realisatie/besluiten/00001-basisstructuur/).
