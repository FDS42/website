---
title: Identiteit
weight: 1
date: 2023-10-17
categories: [Capabilities, Vertrouwen, Identiteit]
tags: 
description: >
  Vertrouwen | Identiteit: Identiteitsbeheer
---

{{< capabilities-diagram selected="identiteit" >}}

## Beschrijving

**Vertrouwen | Identiteit**:

Dit bouwblok biedt identificatie, authenticatie en autorisatie van deelnemers in het federatief datastelsel.
Het verzekert dat organisaties, individuen, machines en andere actoren zijn voorzien van betrouwbare identiteiten en
dat die identiteiten kunnen worden geauthenticeerd en geverifieerd voor autorisatie mechanismen voor toegang en gebruikscontrole.

## Bouwblokken

{{< architecturelayer color="purple" >}}
- Bouwblok Autoriteitmanagement Contract
- EIDAS
{{< /architecturelayer >}}

{{< architecturelayer color="yellow" >}}
- Bouwblok Regisseur
{{< /architecturelayer >}}

{{< architecturelayer color="blue" >}}
- Bouwblok Poortwachter
{{< /architecturelayer >}}

{{< architecturelayer color="blue" >}}
- Bouwblok Selfdescription
{{< /architecturelayer >}}

{{< architecturelayer color="green" >}}
- [Bouwblok FSC standaard](../../bouwblokken/fsc/)
- Bouwblok NLX
- Bouwblok FDS Node
{{< /architecturelayer >}}

