---
title: Beheer
weight: 3
date: 2023-10-17
categories: [Capabilities, Governance, Beheer]
tags: 
description: >
  Governance | Beheer: Continuïteit en doorontwikkeling van het stelsel
---

{{< capabilities-diagram selected="beheer" >}}

**Governance | Beheer**:

Continuïteit en doorontwikkeling van het stelsel ... Regie van het stelsel, etc, etc
