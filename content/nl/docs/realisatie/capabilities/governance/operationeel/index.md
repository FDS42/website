---
title: Operationeel
weight: 1
date: 2023-10-17
categories: [Capabilities, Governance, Operationeel]
tags: []
description: >
  Governance | Operationeel: Operationele overeenkomsten en afspraken
---

{{< capabilities-diagram selected="operationeel" >}}

**Governance | Operationeel**:

Continuïteit en doorontwikkeling van het stelsel ... Regie van het stelsel, etc, etc
